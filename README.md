WE-Module3: Generative AI Coursework

This repository encompasses the coursework and code for the Generative AI classes undertaken within Module 3 of the Women Engineers program, facilitated by Talentsprint and endorsed by Google.

Accomplishments:

- Resolved a computational thinking problem employing GenAI.
- Gained comprehension of and engaged in the Yatzee game with GenAI.
- Developed and executed a testing strategy for the Yahtzee code using GenAI.
- Constructed a Markov chain model for text generation.